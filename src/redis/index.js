import redis from 'redis'

// Redis init
const redisClient = redis.createClient()

redisClient.on('ready', (err) => {
  if (err) {
    console.log(`Error connecting to redis: ${err}`)

    return
  }

  console.log('Connected to redis')
})

module.exports = redisClient
