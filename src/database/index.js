import { Pool } from 'pg'
import SQL from 'sql-template-strings'

// Redis client
import redisClient from '../redis'

// Postgres
const pool = new Pool()

export const queryHeroMapData = (res, next, cacheKey) => {
  const query = {
    text: `SELECT * FROM hero_map_data;`,
  }

  pool.query(query)
    .then((data) => {
      // Cache response in redis
      redisClient.set(cacheKey, JSON.stringify(data.rows))
      redisClient.expire(cacheKey, 60*60*24*30) // 30 days
      
      return res.json({
        data: data.rows,
      })
    })
    .catch((error) => next(error))
}

export const queryWinnersOnMap = (mapId, res, next, cacheKey) => {
  const query = SQL`SELECT hero_map_data.id, hero_map_data.name, count(replay_data.heroId), avg(replay_data.isWinner::int::float4)
    FROM replay_data
    INNER JOIN replays ON (replays.replayId = replay_data.replayId)
    INNER JOIN hero_map_data ON (replay_data.heroid = hero_map_data.id)
    WHERE replays.mapId = ${mapId}
    GROUP BY hero_map_data.id
    HAVING count(replay_data.heroId) > 10
    ORDER BY avg DESC;`

  // Query
  pool.query(query)
    .then((data) => {
      // Cache response in redis
      redisClient.set(cacheKey, JSON.stringify(data.rows))
      redisClient.expire(cacheKey, 60*60*24*30) // 30 days

      // Send response
      return res.json({
        data: data.rows,
      })
    })
    .catch((error) => next(error))
}

export const queryDuos = (heroId, mapId, res, next, cacheKey) => {
  const query = SQL`
    SELECT partnerName.id, partnerName.name, count(hero.heroId), avg(hero.isWinner::int::float4)
    FROM replay_data AS hero
      INNER JOIN replay_data AS partner
        ON hero.replayId = partner.replayId 
          AND hero.isWinner = partner.isWinner 
          AND hero.heroId != partner.heroId`
  
  if (mapId) {
    // Join map data only if necessary
    query.append(SQL`
      INNER JOIN replays ON replays.replayId = hero.replayId
    `)
  }
  
  // Join hero table for names
  query.append(SQL`
      INNER JOIN hero_map_data AS partnerName
        ON partner.heroid = partnerName.id
    WHERE hero.heroId = ${heroId}
  `)

  if (mapId) {
    // Filter by map
    query.append(SQL`
      AND replays.mapId = ${mapId}
    `)
  }

  query.append(SQL`
    GROUP BY partnerName.id, partnerName.name
    HAVING count(partnerName.name) > 10
    ORDER BY avg DESC;`
  )

  pool.query(query)
    .then((data) => {
      // Cache response in redis
      redisClient.set(cacheKey, JSON.stringify(data.rows))
      redisClient.expire(cacheKey, 60*60*24*30) // 30 days

      // Send response
      return res.json({
        data: data.rows,
      })
    })
    .catch((error) => {
      console.log('ERROR!', error)
      return next(error)
    })
}

export const queryCounters = (heroId, mapId, res, next, cacheKey) => {
  const query = SQL`
    SELECT counterName.id, counterName.name, count(hero.heroId), avg(hero.isWinner::int::float4) AS winnerAvg, avg(counter.isWinner::int::float4) AS counterAvg
    FROM replay_data AS hero
      INNER JOIN replay_data AS counter
        ON hero.replayId = counter.replayId 
          AND hero.isWinner != counter.isWinner 
          AND hero.heroId != counter.heroId`

  if (mapId) {
    // Join map data only if necessary
    query.append(SQL`
      INNER JOIN replays ON replays.replayId = hero.replayId
    `)
  }

  // Join hero table for names
  query.append(SQL`
      INNER JOIN hero_map_data AS counterName
        ON counter.heroid = counterName.id
    WHERE hero.heroId = ${heroId}
  `)

  if (mapId) {
    // Filter by map
    query.append(SQL`
      AND replays.mapId = ${mapId}
    `)
  }

  query.append(SQL`
    GROUP BY counterName.id, counterName.name
    HAVING count(counterName.name) > 10
    ORDER BY counterAvg DESC;`
  )

  pool.query(query)
    .then((data) => {
      // Cache response in redis
      redisClient.set(cacheKey, JSON.stringify(data.rows))
      redisClient.expire(cacheKey, 60*60*24*30) // 30 days

      // Send response
      return res.json({
        data: data.rows,
      })
    })
    .catch((error) => next(error))
}
