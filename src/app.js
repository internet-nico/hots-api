import express from 'express'
import path from 'path'
import favicon from 'serve-favicon'
import logger from 'morgan'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import session from 'express-session'
import connectRedis from 'connect-redis'
import env from 'node-env-file'

// Copy .env to process.env
env(`${__dirname}/../.env`)

// Redis client
import redisClient from './redis'

// Routes
import PublicRoutes from './routes/public'
import DefaultRoutes from './routes'

// Init redis session store
const RedisStore = connectRedis(session)

// GA client
import ua from 'universal-analytics'

const visitor = ua(process.env.GA_ACCOUNT)

// Express init
const app = express()

// Security (http://expressjs.com/en/advanced/best-practice-security.html)
app.disable('x-powered-by')

// view engine setup
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(favicon(path.join(__dirname, '../public', 'favicon.ico')))
app.use(logger('dev'))

// Allow both json and url-encoded requests
app.use(bodyParser.json())
// req.body contains the parsed data, this object will contain key-value pairs
// where the value can be a string or array (when extended is false), or any type (when extended is true).
app.use(bodyParser.urlencoded({
  extended: true,
}))

app.use(cookieParser())

// Built-in serve-static module to serve static assets
app.use(express.static(path.join(__dirname, 'public')))

// Session store init (connect-session and connect-redis)
// https://github.com/expressjs/session
app.use(session({
  store: new RedisStore({ client: redisClient }),
  secret: process.env.CONNECT_SESSION_SECRET,
  saveUninitialized: true, // Any user, even non-auth, will get a session entry
  resave: false,
  cookie: {
    maxAge: 86400000 * 7, // 7 days
    HttpOnly: true,
    rolling: true, // reset the expiration countdown every request
    // When rolling is set to true but the saveUninitialized option is set to false, the cookie will not be set on a response with an uninitialized session.
    // secure: true // will only transmit on SSL
  },
}))

// Cross domain middleware (before routes)
app.use((req, res, next) => {
  const allowedOrigins = ['http://localhost:8000', 'http://localhost:8001', 'http://localhost:8080', 'http://localhost:8081', 'http://hots.fun', 'http://www.hots.fun']
  const origin = req.headers.origin

  // Match origin from allowed origins
  if (allowedOrigins.indexOf(origin) > -1)
    res.header('Access-Control-Allow-Origin', origin)

  // res.header('Access-Control-Allow-Origin', 'http://localhost:8080')
  res.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE,OPTIONS') // ,PUT,POST,DELETE')
  res.header('Access-Control-Allow-Headers', 'X-Requested-With, X-Access-Token, Content-Type, Authorization, Content')
  res.header('Access-Control-Allow-Credentials', true)

  // intercept OPTIONS "pre-flight" request
  if (req.method == 'OPTIONS')
    res.sendStatus(200)

  else
    next()
})

// Universal-analytics middleware
app.use('/', (req, res, next) => {
  // Track pageview
  if (visitor)
    visitor.pageview(req.url).send()

  next()
})

// Handle uncaught exceptions?
process.on('uncaughtException', (err) => {
  console.log('Uncaught exception!', err)
})

// Public API
app.use('/public', PublicRoutes)
// Other app routes and error handling
const routes = DefaultRoutes(app)

module.exports = app
