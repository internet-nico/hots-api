import express from 'express'
import async from 'async'
import env from 'node-env-file'

// Redis client
import redisClient from '../redis'

// Database functions
import * as db from '../database'

// Copy .env to process.env
env(`${__dirname}/../../.env`)

const router = express.Router()

const checkRedisCache = (cacheKey, next) => {
  // Check redis first
  redisClient.get(cacheKey, function(err, reply) {
    if (!err && reply) {
      // No error, send response
      return next(JSON.parse(reply))
    }
    else {
      // Nothing found in redis, continue
      return next()
    }
  })
}

router.route('/')
  .get((req, res, next) => res.json({
    success: true,
  }))

router.route('/hero-map-data')
  .get((req, res, next) => {
    const cacheKey = `hots__heroMapData`

    return checkRedisCache(cacheKey, (cacheData) => {
      if (cacheData) {
        // Send cached response
        return res.json({
          data: cacheData,
        })
      }
      else {
        // Perform query
        return db.queryHeroMapData(res, next, cacheKey)
      }
    })
  })

router.route('/winners-on-map/:mapId')
  .get((req, res, next) => {
    const mapId = req.params.mapId
    if (!mapId) {
      return res.json({
        success: false,
        message: 'Invalid input params',
      })
    }

    const cacheKey = `hots__winnersOnMap__${mapId}`

    return checkRedisCache(cacheKey, (cacheData) => {
      if (cacheData) {
        // Send cached response
        return res.json({
          data: cacheData,
        })
      }
      else {
        // Perform query
        return db.queryWinnersOnMap(mapId, res, next, cacheKey)
      }
    })
  })

router.route('/hero-duos/:heroId/:mapId?')
  .get((req, res, next) => {
    const { heroId, mapId } = req.params
    if (!heroId) {
      return res.json({
        success: false,
        message: 'Invalid input params',
      })
    }

    const cacheKey = heroId && mapId ? `hots__heroDuos__${heroId}__${mapId}` : `hots__heroDuos__${heroId}`

    return checkRedisCache(cacheKey, (cacheData) => {
      if (cacheData) {
        // Send cached response
        return res.json({
          data: cacheData,
        })
      }
      else {
        // Perform query
        return db.queryDuos(heroId, mapId, res, next, cacheKey)
      }
    })
  })

router.route('/hero-counters/:heroId/:mapId?')
  .get((req, res, next) => {
    const { heroId, mapId } = req.params
    if (!heroId) {
      return res.json({
        success: false,
        message: 'Invalid input params',
      })
    }

    const cacheKey = heroId && mapId ? `hots__heroCounters__${heroId}__${mapId}` : `hots__heroCounters__${heroId}`

    return checkRedisCache(cacheKey, (cacheData) => {
      if (cacheData) {
        // Send cached response
        return res.json({
          data: cacheData,
        })
      }
      else {
        // Perform query
        return db.queryCounters(heroId, mapId, res, next, cacheKey)
      }
    })
  })

module.exports = router
