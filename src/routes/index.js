import express from 'express'

module.exports = function (app) {
  // Home
  app.get('/', (req, res, next) => {
    res.status(200).json({
      success: true,
      hello: 'world',
    })
  })

  // =====================================
  // 404 =================================
  // =====================================

  // Anything else
  app.get('*', (req, res, next) => {
    console.log('404!')
    res.sendStatus(404)
  })

  // =====================================
  // ERROR HANDLING MIDDLEWARE ===========
  // =====================================

  // catch 404 and forward to error handler
  app.use((req, res, next) => {
    const err = new Error('Not Found')

    err.status = 404
    next(err)
  })

  // error handlers

  // development error handler
  // will print stacktrace
  if (app.get('env') === 'development') {
    console.log('running development')
    app.use((err, req, res, next) => {
      console.log(req.url, err)
      res.status(err.status || 500)
      res.json({
        success: false,
        message: err.message || 'Oops! Something went wrong.',
        error: err,
      })
    })
  }

  // production error handler
  // no stacktraces leaked to user
  app.use((err, req, res, next) => {
    console.log(req.url, err)
    res.status(err.status || 500)
    res.json({
      success: false,
      message: err.message || 'Oops! Something went wrong.',
    })
  })
}
