--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Reset
--

DROP TABLE IF EXISTS hero_map_data CASCADE;
DROP TABLE IF EXISTS replays CASCADE;
DROP TABLE IF EXISTS replay_data CASCADE;
DROP TYPE IF EXISTS GameModeOption CASCADE;

--
-- Public Schema rules
--

GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;

--
-- Hero_Map_Data
--

CREATE TABLE hero_map_data (
  id integer,
  name character varying(42),
  mainGroup character varying(21),
  subGroup character varying(21),
  PRIMARY KEY (id)
);

ALTER TABLE hero_map_data OWNER TO hotsuser;
REVOKE ALL ON TABLE hero_map_data FROM PUBLIC;
GRANT ALL ON TABLE hero_map_data TO hotsuser;

--
-- Replays
--

CREATE TYPE GameModeOption AS ENUM ('3', '4', '5', '6');

CREATE TABLE replays (
  replayId integer,
  gameMode GameModeOption,
  mapId smallint,
  replayLength interval,
  timestamp timestamp,
  PRIMARY KEY (replayId),
  FOREIGN KEY (mapId) REFERENCES hero_map_data(id)
);

ALTER TABLE replays OWNER TO hotsuser;
REVOKE ALL ON TABLE replays FROM PUBLIC;
GRANT ALL ON TABLE replays TO hotsuser;

--
-- Replay_Data
--

CREATE TABLE replay_data (
  id serial,
  replayId integer,
  isAutoSelect integer,
  heroId smallint,
  heroLevel smallint,
  isWinner boolean,
  mmrBefore smallint,
  inGameLevel smallint,
  takedowns smallint,
  killingBlows smallint,
  assists smallint,
  deaths smallint,
  highestKillStreak smallint,
  heroDamage integer,
  siegeDamage integer,
  Healing integer,
  selfHealing integer,
  damageTaken integer,
  experienceContribution integer,
  timeSpentDead interval,
  mercCampCaptures smallint,
  PRIMARY KEY (id),
  FOREIGN KEY (replayId) REFERENCES replays(replayId),
  FOREIGN KEY (heroId) REFERENCES hero_map_data(id)
);

ALTER TABLE replay_data OWNER TO hotsuser;
REVOKE ALL ON TABLE replay_data FROM PUBLIC;
GRANT ALL ON TABLE replay_data TO hotsuser;

--
-- Indexes
--

CREATE INDEX replay_map_index ON replays(mapId);
CREATE INDEX replay_data_heroId_index ON replay_data(heroId);
CREATE INDEX replay_data_heroId ON replay_data(replayId, heroId, isWinner);
