--
-- hero_map_data
--

COPY hero_map_data FROM '/var/www/html/hots-api/database/HeroIDAndMapID.csv' DELIMITER ',' CSV HEADER;

--
-- replays
--

DROP TABLE IF EXISTS tmp_replays CASCADE;
CREATE TEMP TABLE tmp_replays AS SELECT * FROM replays LIMIT 0;

COPY tmp_replays (
  replayId,
  gameMode,
  mapId,
  replayLength,
  timestamp
) FROM '/var/www/html/hots-api/database/Replays.csv' DELIMITER ',' CSV HEADER;

INSERT INTO replays
  SELECT *
  FROM tmp_replays
  WHERE gameMode != '3';

--
-- replay_data
--

DROP TABLE IF EXISTS tmp_replay_data CASCADE;
CREATE TEMP TABLE tmp_replay_data AS SELECT * FROM replay_data LIMIT 0;

COPY tmp_replay_data (
  replayId,
  isAutoSelect,
  heroId,
  heroLevel,
  isWinner,
  mmrBefore,
  inGameLevel,
  takedowns,
  killingBlows,
  assists,
  deaths,
  highestKillStreak,
  heroDamage,
  siegeDamage,
  Healing,
  selfHealing,
  damageTaken,
  experienceContribution,
  timeSpentDead,
  mercCampCaptures
) FROM '/var/www/html/hots-api/database/ReplayCharacters.csv' DELIMITER ',' CSV HEADER;

INSERT INTO replay_data (
  replayId,
  isAutoSelect,
  heroId,
  heroLevel,
  isWinner,
  mmrBefore,
  inGameLevel,
  takedowns,
  killingBlows,
  assists,
  deaths,
  highestKillStreak,
  heroDamage,
  siegeDamage,
  Healing,
  selfHealing,
  damageTaken,
  experienceContribution,
  timeSpentDead,
  mercCampCaptures
)
  SELECT replayId, isAutoSelect, heroId, heroLevel, isWinner, mmrBefore, inGameLevel, takedowns, killingBlows, assists, deaths, highestKillStreak, heroDamage, siegeDamage, Healing, selfHealing, damageTaken, experienceContribution, timeSpentDead, mercCampCaptures
  FROM tmp_replay_data
  WHERE replayId IN (
    SELECT replayId from replays
  )
  AND heroLevel >= 5;

--
-- Clean up
--

DROP TABLE IF EXISTS tmp_replays CASCADE;
DROP TABLE IF EXISTS tmp_replay_data CASCADE;
