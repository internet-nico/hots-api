-- Join map data
select replayId, name
  from replays, hero_map_data
  where replays.mapId = hero_map_data.id
  limit 10;

-- Get heroId/name
select replay_data.heroid, hero_map_data.name
  from replay_data
  inner join replays on (replays.replayId = replay_data.replayId)
  inner join hero_map_data on (replay_data.heroid = hero_map_data.id)
  limit 10;

-- Get winners
select heroId, avg(isWinner::int::float4)
  from replay_data
  group by heroId
  order by avg desc
  limit 10;

-- Get winners on specific map
select hero_map_data.name, count(replay_data.heroId), avg(replay_data.isWinner::int::float4)
  from replay_data 
  inner join replays on (replays.replayId = replay_data.replayId)
  inner join hero_map_data on (replay_data.heroid = hero_map_data.id)
  where replays.mapId = 1001
    -- and replay_data.heroLevel >= 5
    -- and replays.gameMode != '3' -- not quick-match
    -- and replays.timestamp between
    --   now()::date - extract(dow from now())::int - 60 
    --   and now()::date - extract(dow from now())::int
  group by hero_map_data.name
  having count(replay_data.heroId) > 10
  order by avg desc
  limit 10;

-- Get partner for hero on map
SELECT partnerName.name, count(hero.heroId), avg(hero.isWinner::int::float4)
FROM replay_data AS hero
  INNER JOIN replay_data AS partner
    ON hero.replayId = partner.replayId 
      AND hero.isWinner = partner.isWinner 
      AND hero.heroId != partner.heroId
  -- INNER JOIN replays ON replays.replayId = hero.replayId
  INNER JOIN hero_map_data AS partnerName
    ON partner.heroid = partnerName.id
WHERE hero.heroId = 38
  -- AND replays.mapId = 1001
GROUP BY heroName.name, partnerName.name
HAVING count(partnerName.name) > 10
ORDER BY avg DESC
LIMIT 20;

-- Get counter for hero on map
SELECT counterName.name, count(hero.heroId), avg(hero.isWinner::int::float4) AS winnerAvg, avg(counter.isWinner::int::float4) AS counterAvg
FROM replay_data AS hero
  INNER JOIN replay_data AS counter
    ON hero.replayId = counter.replayId 
      AND hero.isWinner != counter.isWinner 
      AND hero.heroId != counter.heroId
  -- INNER JOIN replays ON replays.replayId = hero.replayId
  INNER JOIN hero_map_data AS counterName
    ON counter.heroid = counterName.id
WHERE hero.heroId = 23
  -- AND replays.mapId = 1001
GROUP BY counterName.name
HAVING count(counterName.name) > 10
ORDER BY counterAvg DESC
LIMIT 20;
