# Hots-api
Backend for hots-web

## Project Installation
  - `sudo git clone https://gitlab.com/internet-nico/hots-api.git`
  - `cd hots-api`
  - `sudo git remote set-url origin git@gitlab.com:internet-nico/hots-api.git`
  - install vagrant, ansible
  - `vagrant plugin install vagrant-vbguest`
  - `vagrant vbguest`
  - `vagrant up --provision`
  - `npm install`

### Postgres installation
  - Download hotslogs data
    - `cd database/`
    - `sudo wget https://d1i1jxrdh2kvwy.cloudfront.net/Data/HOTSLogs%20Data%20Export%20Current.zip`
    - `sudo unzip HOTSLogs%20Data%20Export%20Current.zip`
  - Init hotslogs database
    - [tutorial](https://codingbee.net/tutorials/postgresql/postgresql-install-postgresql-and-then-create-a-db-and-user-account)
    - `cd /var/lib/pgsql`
    - `sudo -u postgres /usr/pgsql-9.6/bin/initdb -d /var/lib/pgsql/9.6/data`
    - `sudo -u postgres /usr/pgsql-9.6/bin/createdb hotslogs`
    - `sudo -u postgres psql`
    - `\password postgres` to change password
    - `create user hotsuser with password 'laptop';`
    - `grant all privileges on database "hotslogs" to hotsuser;`
    - `\c hotslogs`
    - `\i database/database_schema.sql`
    - `\i database/copy_tables.sql` or `copy_tables_production.sql`
    - `\q` to quit
    - Uncomment `listen_addresses = '*'` in `/var/lib/pgsql/9.6/data/postgresql.conf`
    - Get address from welcome message after vagrant ssh
    - Add `host all all {MY IP ADDRESS}/32 trust` to `/var/lib/pgsql/9.6/data/pg_hba.conf`
    - `sudo service postgresql-9.6 restart`
    - `psql -h 127.0.0.1 -U hotsuser -d hotslogs` to connect with correct user or use pgAdmin
  - Init hots-parser database
    - `sudo -u postgres psql`
    - `CREATE DATABASE "hots-parser";`
    - `CREATE ROLE hotsuser WITH LOGIN PASSWORD 'laptop';`
    - `REVOKE CONNECT ON DATABASE "hots-parser" FROM PUBLIC;`
    - `GRANT ALL ON DATABASE "hots-parser" TO hotsuser;`
    - `\q`
    - sudo -u postgres psql -d "hots-parser" -a -f hots-parser/database/database_schema.sql

### Redis Installation
  - none

## Development
  - `npm run dev`

## Production
  - `npm run build`
  - `npm run production-pm2`

## Update hots data
  - `cd database/`
  - `sudo wget https://d1i1jxrdh2kvwy.cloudfront.net/Data/HOTSLogs%20Data%20Export%20Current.zip`
  - `sudo unzip HOTSLogs\ Data\ Export\ Current.zip`
  - `sudo -u postgres psql hotslogs`
  - `\i database_schema.sql`
  - `\i copy_tables.sql` or `copy_tables_production.sql`
  - `\q`

## PGSQL Info
  - `\l` to list dbs
  - `\c` to connect to a db
  - `\d` to describe db or table
  - `\i` to read file
  - `\q` to quit
  - Restart postgres and run `VACUUM FULL hotslogs` to reclaim space after drops

## Redis Info
  - `redis-cli` to connect
  - `set key1 "hello"`
  - `del key1`
  - `keys *` to list keys
  - Clear endpoint caches
    - `redis-cli --scan --pattern 'hots__*' | xargs redis-cli del`
