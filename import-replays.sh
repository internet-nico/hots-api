# Import our replay files
for file in ./_replays/*
do
  echo "parsing $file"
  python hots-parser/main.py "$file"
done